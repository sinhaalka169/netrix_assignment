import { Component, OnInit } from '@angular/core';
import { CookieService } from "ngx-cookie-service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-inner-page',
  templateUrl: './inner-page.component.html',
  styleUrls: ['./inner-page.component.css']
})
export class InnerPageComponent implements OnInit {
  list = ['item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8', 'item9', 'item10'];
  listNew = [];
  cartItem = [];
  err = '';
  showitem: boolean = false;
  check: boolean = false;
  itemCount = 0;
  constructor(
    private cookieService: CookieService,
    private router: Router) { }

  ngOnInit(): void {
    this.removecart();
  }
cart(data) {
  if(localStorage.getItem('data')) {
    this.listNew = localStorage.getItem('data').split(",");
   if(this.listNew.length<3) {
    let newval = localStorage.getItem('data') + ',' + data;
    localStorage.setItem('data',newval);
    this.listNew = localStorage.getItem('data').split(",");
    this.itemCount = this.listNew.length;
   } else {
     this.err = 'Max 3 items allowed in cart';
     console.log('max 3 allowed')
   }
  } else {
    localStorage.setItem('data',data);
    this.listNew = data;
    this.itemCount = 1;
    this.check = true
    console.log(this.listNew);
  }
}
showCartItem() {
  this.showitem = true;
  this.cartItem = this.listNew
  console.log(this.cartItem);
}
removecart() {
  localStorage.removeItem('data');
  this.itemCount = 0;
  this.listNew = [];
  this.cartItem = [];
  this.err = ''
  this.showitem = false
}
logout() {
 this.cookieService.delete('_dite');
 this.cookieService.delete('name');
 this.cookieService.delete('password');
 this.router.navigate(['']);
}
}
