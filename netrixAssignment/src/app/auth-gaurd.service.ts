import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdService implements CanActivate{

  constructor(
    private sharedService: SharedService,
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, 
    state: RouterStateSnapshot) {
console.log(this.sharedService.isAuthenticate());
    if (this.sharedService.isAuthenticate())
      {return true;} else {
   this.router.navigate(['']);
   return false;
      }
  }
}
