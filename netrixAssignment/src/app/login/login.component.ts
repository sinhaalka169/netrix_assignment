import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { SharedService } from './../shared.service';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  msg = '';
  constructor(
    private sharedService: SharedService,
      private router: Router){
        console.log(this.usernameFormControl);
      }

  ngOnInit(): void {
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  usernameFormControl = new FormControl ('', [
    Validators.required,
  ]);
  passFormControl = new FormControl ('', [
    Validators.required,
  ]);
  matcher = new MyErrorStateMatcher();

  check()
  { 
    console.log(this.usernameFormControl);
    var output = this.sharedService.userSignin(this.usernameFormControl.value, this.passFormControl.value);
    if(output == true)
    {this.sharedService.setUserData(output);
      this.router.navigate(['/inner']);
      console.log('user varified');
    }
    else{
this.msg ='Invalid username or password';
console.log('error');
    }
}
}


