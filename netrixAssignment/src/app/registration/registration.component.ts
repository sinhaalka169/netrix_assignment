import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { SharedService } from './../shared.service';
import { CookieService } from "ngx-cookie-service";


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(
    private cookieService: CookieService,
    private sharedService: SharedService,
      private router: Router){
        console.log(this.usernameFormControl);
      }

  ngOnInit(): void {
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  usernameFormControl = new FormControl ('', [
    Validators.required,
  ]);
  passFormControl = new FormControl ('', [
    Validators.required,
  ]);
  matcher = new MyErrorStateMatcher();

signUp() {
    this.cookieService.set('name', this.usernameFormControl.value);
    this.cookieService.set('password', this.passFormControl.value);
    this.router.navigate(['']);
}
}


