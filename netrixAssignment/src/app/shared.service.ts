import { Injectable } from '@angular/core';
import { CookieService } from "ngx-cookie-service";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  isLoggedin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  
  constructor(
    private cookieService: CookieService) { }
  /*
   *@parm userdata came after login
   */

   setUserData(useraData: any) {
    this.cookieService.set("_dite", useraData);
    this.isLoggedin.next(true);
  }

  getUserData() {
    return this.cookieService.get("_dite") || null;
  }

  isAuthenticate(): boolean {
    if (this.cookieService.get("_dite")) {
      this.isLoggedin.next(true);
      return true;
    } else {
      this.isLoggedin.next(false);
      return false;
    }
  }

  userSignin(uname: string, pwd : string)
  {
if(uname == "admin" && pwd =="admin123"){
  console.log('userSignin');
  localStorage.setItem('username',"admin");
  return true;
  } else if(uname == this.cookieService.get('name') && pwd == this.cookieService.get('password') && uname !== '' && pwd !== '')
  {
    console.log('userSigninmmmmmm');
  localStorage.setItem('username',"admin");
  return true;
  
  } else return false;
}
}
